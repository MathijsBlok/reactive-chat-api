package com.mathijsblok.chat

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketMessage
import org.springframework.web.reactive.socket.WebSocketSession
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter
import reactor.core.publisher.ReplayProcessor

@SpringBootApplication
class ChatApplication

fun main(args: Array<String>) {
    runApplication<ChatApplication>(*args)
}

data class Message(val user: String, val content: String)

@Configuration
class WebSocketConfig {

    private val mapper = jacksonObjectMapper()
    private val processor: ReplayProcessor<Message> = ReplayProcessor.create()

    @Bean
    fun handlerMapping(): HandlerMapping {
        val mapping = SimpleUrlHandlerMapping()
        mapping.urlMap = hashMapOf("messages" to messageHandler())
        mapping.order = 10
        return mapping
    }

    fun messageHandler(): WebSocketHandler {
        return WebSocketHandler { session: WebSocketSession ->
            session
                    .send(processor
                            .map(mapper::writeValueAsString)
                            .map(session::textMessage)
                    )
                    .and {
                        session.receive()
                                .map(WebSocketMessage::getPayloadAsText)
                                .map { message -> mapper.readValue<Message>(message) }
                                .subscribe(processor::onNext)
                    }
        }
    }

    @Bean
    fun handlerAdapter(): WebSocketHandlerAdapter {
        return WebSocketHandlerAdapter()
    }
}